import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientComponent } from './register-form/client/client.component';
import { ProductComponent } from './register-form/product/product.component';
import { ProviderComponent } from './register-form/provider/provider.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ClientComponent,
    ProductComponent,
    ProviderComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class FormModule { }
