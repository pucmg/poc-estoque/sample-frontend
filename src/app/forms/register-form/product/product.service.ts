import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/httpService/http-service.service';
import { ProductModel } from 'src/app/models/product.model';
import { ProductSearchModel } from 'src/app/models/productSearch.model';
import { GlobalConf } from 'src/app/common/conf/global';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  product: ProductModel = new ProductModel();
  products: ProductModel[] = []
  productSearch: ProductSearchModel = new ProductSearchModel();

  constructor(private http: HttpService, private conf:GlobalConf) {}

  getProducts(event: Event){
    event.preventDefault();
    const url = `${this.conf.getEndpoints().product}s`
    this.http.doGet(url).subscribe((response: any) => {
      if(response){
        if(response.status == 200){
          response = response.body;
          this.products = response;
          console.log('Mercadorias obtidos.');
        }else{
          console.log('Falha em obeter mercadorias - ', response);
        }
      }else{
        console.log('Falha em obter mercadorias - Nada retornado.');
      }
    })
  }

  getProductsByName(event: Event){
    event.preventDefault();
    const url = `${this.conf.getEndpoints().product}s/name/${this.productSearch.name}`
    this.http.doGet(url).subscribe((response: any) => {
      if(response){
        if(response.status == 200){
          response = response.body;
          this.products = response;
          console.log('Mercadorias obtidos.', this.productSearch);
        }else{
          console.log('Falha em obeter mercadorias - ', response, this.productSearch);
        }
      }else{
        console.log('Falha em obter mercadorias - Nada retornado.', this.productSearch);
      }
    })
  }

  createProduct(event: Event){
    event.preventDefault();
    var body = {
      sku: this.product.sku,
      name: this.product.name,
      description: this.product.description,
      weight: this.product.weight
    }
    this.http.doPost(this.conf.getEndpoints().product, body).subscribe((response: any) => {
      if(response){
        if(response.status == 201){
          response = response.body;
          const product = new ProductModel();
          product.sku = response.sku;
          product.name = response.name;
          product.description = response.description;
          product.weight = response.weight;
          this.products.push(product);
          console.log('Mercadorias criado: ', response);
        }else{
          console.log('Falha em obeter mercadorias - ', response);
        }
      }else{
        console.log('Falha em obter mercadorias - Nada retornado.');
      }
    })
  }

  updateProduct(event: Event){
    event.preventDefault();
    const url = `${this.conf.getEndpoints().product}`;
    this.http.doPut(url, this.product).subscribe((response: any) => {
      if(response){
        if(response.status == 200){
          console.log('Mercadoria atualizado.', response);
        }else{
          console.log('Falha na atualização - ', response);
        }
      }else{
        console.log('Falha na atualização - Nada retornado.');
      }
    })
  }

  deleteProduct(event: Event){
    event.preventDefault();
    const url = `${this.conf.getEndpoints().product}/${this.product.id}`;
    this.http.doDelete(url, this.product).subscribe((response: any) => {
      if(response){
        if(response.status == 200){
          console.log('Mercadoria removido.', response);
        }else{
          console.log('Falha na remoção - ', response);
        }
      }else{
        console.log('Falha na remoção - Nada retornado.');
      }

    })
  }
}
