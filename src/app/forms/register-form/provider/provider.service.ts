import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/httpService/http-service.service';
import { ProviderModel } from 'src/app/models/provider.model';
import { ProviderSearchModel } from 'src/app/models/providerSearch.model';
import { GlobalConf } from 'src/app/common/conf/global';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {
  provider: ProviderModel = new ProviderModel();
  providers: ProviderModel[] = [];
  providerSearch: ProviderSearchModel = new ProviderSearchModel();

  constructor(private http: HttpService, private conf:GlobalConf) {}

  getProviders(event: Event){
    event.preventDefault();
    const url = `${this.conf.getEndpoints().provider}s`
    this.http.doGet(url).subscribe((response: any) => {
      if(response){
        if(response.status == 200){
          response = response.body;
          this.providers = response;
          console.log('Fornecedor obtidos.');
        }else{
          console.log('Falha em obeter fornecedor - ', response);
        }
      }else{
        console.log('Falha em obter fornecedor - Nada retornado.');
      }
    })
  }

  getProvidersByPublicName(event: Event){
    event.preventDefault();
    const url = `${this.conf.getEndpoints().provider}s/publicName/${this.providerSearch.publicName}`
    this.http.doGet(url).subscribe((response: any) => {
      if(response){
        if(response.status == 200){
          response = response.body;
          this.providers = response;
          console.log('Fornecedores obtidos.', this.providerSearch);
        }else{
          console.log('Falha em obeter fornecedores - ', response, this.providerSearch);
        }
      }else{
        console.log('Falha em obter fornecedores - Nada retornado.');
      }
    })
  }

  createProvider(event: Event){
    event.preventDefault();
    var body = {
      legalName: this.provider.legalName,
      publicName: this.provider.publicName,
      document: this.provider.document,
      mainEmail: this.provider.mainEmail,
      specialization: this.provider.specialization,
      location: this.provider.location
    }
    this.http.doPost(this.conf.getEndpoints().provider, body).subscribe((response: any) => {
      if(response){
        if(response.status == 201){
          response = response.body;
          const provider = new ProviderModel();
          provider.legalName = response.legalName;
          provider.publicName = response.publicName;
          provider.document = response.document;
          provider.specialization = response.specialization;
          provider.location = response.location;
          this.providers.push(provider);
          console.log('Fornecedor criado: ', response);
        }else{
          console.log('Falha em obeter fornecedor - ', response);
        }
      }else{
        console.log('Falha em obter fornecedor - Nada retornado.');
      }
    })
  }

  updateProvider(event: Event){
    event.preventDefault();
    const url = `${this.conf.getEndpoints().provider}`;
    this.http.doPut(url, this.provider).subscribe((response: any) => {
      if(response){
        if(response.status == 200){
          console.log('Fornecedor atualizado.', response);
        }else{
          console.log('Falha na atualização - ', response);
        }
      }else{
        console.log('Falha na atualização - Nada retornado.');
      }
    })
  }

  deleteProvider(event: Event){
    event.preventDefault();
    const url = `${this.conf.getEndpoints().provider}/${this.provider.id}`;
    this.http.doDelete(url, this.provider).subscribe((response: any) => {
      if(response){
        if(response.status == 200){
          console.log('Fornecedor removido.', response);
        }else{
          console.log('Falha na remoção - ', response);
        }
      }else{
        console.log('Falha na remoção - Nada retornado.');
      }

    })
  }
}
