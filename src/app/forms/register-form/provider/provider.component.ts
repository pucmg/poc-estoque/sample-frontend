import { Component, OnInit } from '@angular/core';
import { ProviderService } from './provider.service';

@Component({
  selector: 'app-provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.css']
})
export class ProviderComponent implements OnInit {

  providerService: ProviderService

  constructor(providerService: ProviderService) {
    this.providerService = providerService
   }

  ngOnInit(): void {
  }

}
