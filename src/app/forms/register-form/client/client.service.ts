import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/httpService/http-service.service';
import { ClientModel } from 'src/app/models/client.model';
import { ClientSearchModel } from 'src/app/models/clientSearch.model';
import { GlobalConf } from 'src/app/common/conf/global';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  client: ClientModel = new ClientModel()
  clients: ClientModel[] = []
  clientSearch: ClientSearchModel = new ClientSearchModel();

  constructor(private http: HttpService, private conf:GlobalConf) {}

  getClients(event: Event){
    event.preventDefault();
    const url = `${this.conf.getEndpoints().client}s`
    this.http.doGet(url).subscribe((response: any) => {
      if(response){
        if(response.status == 200){
          response = response.body;
          this.clients = response;
          console.log('Clientes obtidos.');
        }else{
          console.log('Falha em obeter clientes - ', response);
        }
      }else{
        console.log('Falha em obter clientes - Nada retornado.');
      }
    })
  }

  getClientsByName(event: Event){
    event.preventDefault();
    const url = `${this.conf.getEndpoints().client}s/name/${this.clientSearch.name}`
    this.http.doGet(url).subscribe((response: any) => {
      if(response){
        if(response.status == 200){
          response = response.body;
          this.clients = response;
          console.log('Clientes obtidos.', this.clientSearch);
        }else{
          console.log('Falha em obeter clientes - ', response, this.clientSearch);
        }
      }else{
        console.log('Falha em obter clientes - Nada retornado.');
      }
    })
  }

  createClient(event: Event){
    event.preventDefault();
    var body = {
      name: this.client.name,
      document: this.client.document,
      email: this.client.email,
      address: this.client.address
    }
    this.http.doPost(this.conf.getEndpoints().client, body).subscribe((response: any) => {
      if(response){
        if(response.status == 201){
          response = response.body;
          const client = new ClientModel();
          client.name = response.name;
          client.document = response.document;
          client.email = response.email;
          client.address = response.address;
          this.clients.push(client);
          console.log('Clientes criado: ', response);
        }else{
          console.log('Falha em obeter clientes - ', response);
        }
      }else{
        console.log('Falha em obter clientes - Nada retornado.');
      }
    })
  }

  updateClient(event: Event){
    event.preventDefault();
    const url = `${this.conf.getEndpoints().client}`;
    this.http.doPut(url, this.client).subscribe((response: any) => {
      if(response){
        if(response.status == 200){
          console.log('Cliente atualizado.', response);
        }else{
          console.log('Falha na atualização - ', response);
        }
      }else{
        console.log('Falha na atualização - Nada retornado.');
      }
    })
  }

  deleteClient(event: Event){
    event.preventDefault();
    const url = `${this.conf.getEndpoints().client}/${this.client.id}`;
    this.http.doDelete(url, this.client).subscribe((response: any) => {
      if(response){
        if(response.status == 200){
          console.log('Cliente removido.', response);
        }else{
          console.log('Falha na remoção - ', response);
        }
      }else{
        console.log('Falha na remoção - Nada retornado.');
      }

    })
  }

}
