import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http"
import { Observable } from 'rxjs';
import { GlobalConf } from '../common/conf/global';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient, private conf:GlobalConf) {}

  setHeader(header?: object) {
    var ret = {'Content-Type':'application/json; charset=utf-8'};
    if(typeof header != 'undefined'){
      ret = Object.assign({},ret,header);
    }

    var token = this.conf.getToken();
    console.log(token);
    if(token != ''){
      ret = Object.assign({},ret,{'Authorization': 'Bearer ' + token})
    }

    return new HttpHeaders(ret);
  }

  doGet(url: string, header?: object){
    return this.http.get(
      url, { headers: this.setHeader(header), observe: 'response' }
    );
  }

  doPost(url: string, content: any, header?: object): Observable<any>{
    return this.http.post(
      url, JSON.stringify(content), { headers: this.setHeader(header), observe: 'response' }
    )
  }

  doPatch(url: string, content: any, header?: object): Observable<any>{
    return this.http.patch(
      url, JSON.stringify(content), { headers: this.setHeader(header), observe: 'response' }
    )
  }

  doPut(url: string, content: any, header?: object): Observable<any>{
    return this.http.put(
      url, JSON.stringify(content), { headers: this.setHeader(header), observe: 'response' }
    )
  }

  doDelete(url: string, content: any, header?: object): Observable<any>{
    return this.http.delete(
      url, { headers: this.setHeader(header), observe: 'response' }
    )
  }

}
