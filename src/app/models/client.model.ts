export class ClientModel {
  id: number = 0;
  name: string = "";
  document: number  = 0;
  email: string  = "";
  address: string  = "";
}
