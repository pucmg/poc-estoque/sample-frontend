export class ProductModel {
  id: number = 0;
  sku: number = 0;
  name: string  = "";
  description: string  = "";
  weight: number  = 0;
}
