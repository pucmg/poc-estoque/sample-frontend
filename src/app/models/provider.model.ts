export class ProviderModel {
  id: number = 0;
  legalName: string = "";
  publicName: string = "";
  document: number = 0;
  mainEmail: string = "";
  specialization: string = "";
  location: string = "";
}
