import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientComponent } from './forms/register-form/client/client.component';
import { ProductComponent } from './forms/register-form/product/product.component';
import { ProviderComponent } from './forms/register-form/provider/provider.component';

const routes: Routes = [
  { path: 'register/client', component: ClientComponent },
  { path: 'register/product', component: ProductComponent },
  { path: 'register/provider', component: ProviderComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
