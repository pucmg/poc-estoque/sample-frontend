import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class GlobalConf {

  constructor() {}

  private sampleUser = { "username": 'colaborador', "password": '123', "grant_type": 'info-register'};
  private currentToken = {authorization_type: '', token: ''};

  private endpoints = {
    /* ms-infocad - Módulo de Informações cadastrais */
    "client": "ms-infocad/api/v1/client",
    "provider": "ms-infocad/api/v1/provider",
    "product": "ms-infocad/api/v1/product",
    /* ms-authserver - Módulo de autenticação */
    "user": 'ms-authserver/api/v1/user',
    "getToken": 'ms-authserver/api/v1/auth/token',
    "validateToken": 'ms-authserver/api/v1/auth/validate'
  };

  public getEndpoints(){
    return this.endpoints;
  }

  public getSampleUser(){
    return this.sampleUser;
  }

  public getToken(){
    return this.currentToken.token;
  }

  public setToken(new_value:string){
    return this.currentToken.token = new_value;
  }

  public getAuthType(){
    return this.currentToken.authorization_type;
  }

  public setAuthType(new_value:string){
    return this.currentToken.authorization_type = new_value;
  }

}
