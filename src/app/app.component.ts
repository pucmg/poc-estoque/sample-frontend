import { Component, OnInit } from '@angular/core';
import { HttpService } from './httpService/http-service.service';
import { GlobalConf } from './common/conf/global';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'sample-frontend';

  constructor(private http: HttpService, private conf:GlobalConf) {
  }

  ngOnInit() {
    this.http.doGet(this.conf.getEndpoints().getToken, this.conf.getSampleUser()).subscribe((response: any) => {
      if(response != null){
        response = response.body;
        this.conf.setAuthType(response.authorization_type);
        this.conf.setToken(response.token);
        console.log('Sessão aberta: ', response.token);
      } else {
        console.log('Error appNgOnInit: sampleUser without opened session. Please register sampleUser {colaborador,123}');
      }
    })
  }
}
