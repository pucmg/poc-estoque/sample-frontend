# SAMPLE-FRONTEND

Frontend simplista, em angular, para demonstração de uso da POC de microserviço ms-infocad ao projeto final de arquitetura de sistemas distribuídos da PUC-MG, pelos alunos Felipe Almeida da Graça e Pedro Henrique Ribeiro Lopes

## Requisitos para execução em ambiente local de demonstração

- Git instalado (para instruções, conforme seu ambiente operacional: [clique aqui](https://git-scm.com/downloads));
- ms-infocad em execução (para tal, veja respectivo [readme.me](https://gitlab.com/pucmg/poc-estoque/ms-infocad/-/blob/main/README.md));
- ms-authserver em execução (para tal, veja respectivo [readme.me](https://gitlab.com/pucmg/poc-estoque/ms-authserver/-/blob/main/README.md));
- Node.js instalado (para instruções, conforme seu ambiente operacional: [clique aqui](https://nodejs.org/pt-br/download/package-manager/));
- npm package manager (para instruções, conforme seu ambiente operacional: [clique aqui](https://docs.npmjs.com/cli/v9/commands/npm-install))
- angular-cli instalado (para instruções, conforme seu ambiente operacional: [clique aqui](https://angular.io/guide/setup-local));
- angular-devkit instalado (para instruções, conforme seu ambiente operacional: [clique aqui](https://www.npmjs.com/package/@angular-devkit/build-angular));
- Baixe este projeto a um diretório em seu ambiente local (para instruções de como clonar um projeto com git: [clique aqui](https://www.youtube.com/watch?v=WEPB5pDSEIg));

## Execução no ambiente local de demonstração

No diretório da aplicação sample-frontend, em sua interface de terminal de preferência, execute:

- Para compilar o projeto (normalmente na primeira execução apenas):
`npm run build`

- Em sequência da compilação, para execução:
`npm run start`

Ao final da inicialização será exibida a mensagem ``. Neste momento, basta abrir seu browser de preferência na URL `http://localhost:4200/` e navegar pelas rotas da aplicação para os testes.

## Observações
1. Como está solução tem fins meramente acadêmicos, fluxos de validação de erro, ou tratamentos visuais mais elaborados não foram implementados, uma vez que não seriam necessários para validação da POC.
2. Como o objetivo desta demonstração é acadêmico e, não com intuito de cobrir todo o fluxo sistêmico fim-a-fim da solução, o sample-frontend faz acesso aos microserviços via proxy, diretamente, ao invés de usarmos um api gateway, como proposto para ambiente produtivo.
3. Do mesmo modo do item 2, o ms-authserver já dispõe de um usuário de demonstração, cujo login é colaborador e a senha é 123, o qual é usando na inicialização do app para abertura de sessão (vide ngOnInit do componente App).
